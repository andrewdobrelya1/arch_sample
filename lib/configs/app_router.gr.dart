// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'app_router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    BreedsRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const BreedsScreen(),
      );
    },
    BreedInfoRoute.name: (routeData) {
      final args = routeData.argsAs<BreedInfoRouteArgs>();
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: BreedInfoScreen(
          breedInfo: args.breedInfo,
          key: args.key,
        ),
      );
    },
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(
          BreedsRoute.name,
          path: '/',
        ),
        RouteConfig(
          BreedInfoRoute.name,
          path: 'breed_info',
        ),
      ];
}

/// generated route for
/// [BreedsScreen]
class BreedsRoute extends PageRouteInfo<void> {
  const BreedsRoute()
      : super(
          BreedsRoute.name,
          path: '/',
        );

  static const String name = 'BreedsRoute';
}

/// generated route for
/// [BreedInfoScreen]
class BreedInfoRoute extends PageRouteInfo<BreedInfoRouteArgs> {
  BreedInfoRoute({
    required BreedInfo breedInfo,
    Key? key,
  }) : super(
          BreedInfoRoute.name,
          path: 'breed_info',
          args: BreedInfoRouteArgs(
            breedInfo: breedInfo,
            key: key,
          ),
        );

  static const String name = 'BreedInfoRoute';
}

class BreedInfoRouteArgs {
  const BreedInfoRouteArgs({
    required this.breedInfo,
    this.key,
  });

  final BreedInfo breedInfo;

  final Key? key;

  @override
  String toString() {
    return 'BreedInfoRouteArgs{breedInfo: $breedInfo, key: $key}';
  }
}
