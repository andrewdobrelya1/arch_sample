import 'package:arch_sample/app_entry.dart';
import 'package:arch_sample/dependencies.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:arch_sample/firebase_options.dart';

Future<void> main() async {
  setupServiceLocator();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(AppEntry());
}
