// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'breeds_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$BreedsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadedEvent value) loaded,
    required TResult Function(ShowInfoEvent value) showInfo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadedEvent value)? loaded,
    TResult? Function(ShowInfoEvent value)? showInfo,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadedEvent value)? loaded,
    TResult Function(ShowInfoEvent value)? showInfo,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BreedsEventCopyWith<$Res> {
  factory $BreedsEventCopyWith(
          BreedsEvent value, $Res Function(BreedsEvent) then) =
      _$BreedsEventCopyWithImpl<$Res, BreedsEvent>;
}

/// @nodoc
class _$BreedsEventCopyWithImpl<$Res, $Val extends BreedsEvent>
    implements $BreedsEventCopyWith<$Res> {
  _$BreedsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadedEventCopyWith<$Res> {
  factory _$$LoadedEventCopyWith(
          _$LoadedEvent value, $Res Function(_$LoadedEvent) then) =
      __$$LoadedEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadedEventCopyWithImpl<$Res>
    extends _$BreedsEventCopyWithImpl<$Res, _$LoadedEvent>
    implements _$$LoadedEventCopyWith<$Res> {
  __$$LoadedEventCopyWithImpl(
      _$LoadedEvent _value, $Res Function(_$LoadedEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadedEvent implements LoadedEvent {
  const _$LoadedEvent();

  @override
  String toString() {
    return 'BreedsEvent.loaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadedEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
  }) {
    return loaded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
  }) {
    return loaded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadedEvent value) loaded,
    required TResult Function(ShowInfoEvent value) showInfo,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadedEvent value)? loaded,
    TResult? Function(ShowInfoEvent value)? showInfo,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadedEvent value)? loaded,
    TResult Function(ShowInfoEvent value)? showInfo,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedEvent implements BreedsEvent {
  const factory LoadedEvent() = _$LoadedEvent;
}

/// @nodoc
abstract class _$$ShowInfoEventCopyWith<$Res> {
  factory _$$ShowInfoEventCopyWith(
          _$ShowInfoEvent value, $Res Function(_$ShowInfoEvent) then) =
      __$$ShowInfoEventCopyWithImpl<$Res>;
  @useResult
  $Res call({BreedInfo breedInfo});
}

/// @nodoc
class __$$ShowInfoEventCopyWithImpl<$Res>
    extends _$BreedsEventCopyWithImpl<$Res, _$ShowInfoEvent>
    implements _$$ShowInfoEventCopyWith<$Res> {
  __$$ShowInfoEventCopyWithImpl(
      _$ShowInfoEvent _value, $Res Function(_$ShowInfoEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? breedInfo = null,
  }) {
    return _then(_$ShowInfoEvent(
      breedInfo: null == breedInfo
          ? _value.breedInfo
          : breedInfo // ignore: cast_nullable_to_non_nullable
              as BreedInfo,
    ));
  }
}

/// @nodoc

class _$ShowInfoEvent implements ShowInfoEvent {
  const _$ShowInfoEvent({required this.breedInfo});

  @override
  final BreedInfo breedInfo;

  @override
  String toString() {
    return 'BreedsEvent.showInfo(breedInfo: $breedInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowInfoEvent &&
            (identical(other.breedInfo, breedInfo) ||
                other.breedInfo == breedInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, breedInfo);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShowInfoEventCopyWith<_$ShowInfoEvent> get copyWith =>
      __$$ShowInfoEventCopyWithImpl<_$ShowInfoEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
  }) {
    return showInfo(breedInfo);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
  }) {
    return showInfo?.call(breedInfo);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    required TResult orElse(),
  }) {
    if (showInfo != null) {
      return showInfo(breedInfo);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadedEvent value) loaded,
    required TResult Function(ShowInfoEvent value) showInfo,
  }) {
    return showInfo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadedEvent value)? loaded,
    TResult? Function(ShowInfoEvent value)? showInfo,
  }) {
    return showInfo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadedEvent value)? loaded,
    TResult Function(ShowInfoEvent value)? showInfo,
    required TResult orElse(),
  }) {
    if (showInfo != null) {
      return showInfo(this);
    }
    return orElse();
  }
}

abstract class ShowInfoEvent implements BreedsEvent {
  const factory ShowInfoEvent({required final BreedInfo breedInfo}) =
      _$ShowInfoEvent;

  BreedInfo get breedInfo;
  @JsonKey(ignore: true)
  _$$ShowInfoEventCopyWith<_$ShowInfoEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$BreedsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BreedsStateCopyWith<$Res> {
  factory $BreedsStateCopyWith(
          BreedsState value, $Res Function(BreedsState) then) =
      _$BreedsStateCopyWithImpl<$Res, BreedsState>;
}

/// @nodoc
class _$BreedsStateCopyWithImpl<$Res, $Val extends BreedsState>
    implements $BreedsStateCopyWith<$Res> {
  _$BreedsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialStateCopyWith<$Res> {
  factory _$$_InitialStateCopyWith(
          _$_InitialState value, $Res Function(_$_InitialState) then) =
      __$$_InitialStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialStateCopyWithImpl<$Res>
    extends _$BreedsStateCopyWithImpl<$Res, _$_InitialState>
    implements _$$_InitialStateCopyWith<$Res> {
  __$$_InitialStateCopyWithImpl(
      _$_InitialState _value, $Res Function(_$_InitialState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_InitialState implements _InitialState {
  const _$_InitialState();

  @override
  String toString() {
    return 'BreedsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_InitialState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _InitialState implements BreedsState {
  const factory _InitialState() = _$_InitialState;
}

/// @nodoc
abstract class _$$_LoadingStateCopyWith<$Res> {
  factory _$$_LoadingStateCopyWith(
          _$_LoadingState value, $Res Function(_$_LoadingState) then) =
      __$$_LoadingStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingStateCopyWithImpl<$Res>
    extends _$BreedsStateCopyWithImpl<$Res, _$_LoadingState>
    implements _$$_LoadingStateCopyWith<$Res> {
  __$$_LoadingStateCopyWithImpl(
      _$_LoadingState _value, $Res Function(_$_LoadingState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_LoadingState implements _LoadingState {
  const _$_LoadingState();

  @override
  String toString() {
    return 'BreedsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_LoadingState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _LoadingState implements BreedsState {
  const factory _LoadingState() = _$_LoadingState;
}

/// @nodoc
abstract class _$$_LoadedStateCopyWith<$Res> {
  factory _$$_LoadedStateCopyWith(
          _$_LoadedState value, $Res Function(_$_LoadedState) then) =
      __$$_LoadedStateCopyWithImpl<$Res>;
  @useResult
  $Res call({List<BreedInfo> fetchedDogs});
}

/// @nodoc
class __$$_LoadedStateCopyWithImpl<$Res>
    extends _$BreedsStateCopyWithImpl<$Res, _$_LoadedState>
    implements _$$_LoadedStateCopyWith<$Res> {
  __$$_LoadedStateCopyWithImpl(
      _$_LoadedState _value, $Res Function(_$_LoadedState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fetchedDogs = null,
  }) {
    return _then(_$_LoadedState(
      fetchedDogs: null == fetchedDogs
          ? _value._fetchedDogs
          : fetchedDogs // ignore: cast_nullable_to_non_nullable
              as List<BreedInfo>,
    ));
  }
}

/// @nodoc

class _$_LoadedState implements _LoadedState {
  const _$_LoadedState({required final List<BreedInfo> fetchedDogs})
      : _fetchedDogs = fetchedDogs;

  final List<BreedInfo> _fetchedDogs;
  @override
  List<BreedInfo> get fetchedDogs {
    if (_fetchedDogs is EqualUnmodifiableListView) return _fetchedDogs;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_fetchedDogs);
  }

  @override
  String toString() {
    return 'BreedsState.loaded(fetchedDogs: $fetchedDogs)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoadedState &&
            const DeepCollectionEquality()
                .equals(other._fetchedDogs, _fetchedDogs));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_fetchedDogs));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoadedStateCopyWith<_$_LoadedState> get copyWith =>
      __$$_LoadedStateCopyWithImpl<_$_LoadedState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) {
    return loaded(fetchedDogs);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) {
    return loaded?.call(fetchedDogs);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(fetchedDogs);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _LoadedState implements BreedsState {
  const factory _LoadedState({required final List<BreedInfo> fetchedDogs}) =
      _$_LoadedState;

  List<BreedInfo> get fetchedDogs;
  @JsonKey(ignore: true)
  _$$_LoadedStateCopyWith<_$_LoadedState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ShowInfoCopyWith<$Res> {
  factory _$$_ShowInfoCopyWith(
          _$_ShowInfo value, $Res Function(_$_ShowInfo) then) =
      __$$_ShowInfoCopyWithImpl<$Res>;
  @useResult
  $Res call({BreedInfo breedInfo});
}

/// @nodoc
class __$$_ShowInfoCopyWithImpl<$Res>
    extends _$BreedsStateCopyWithImpl<$Res, _$_ShowInfo>
    implements _$$_ShowInfoCopyWith<$Res> {
  __$$_ShowInfoCopyWithImpl(
      _$_ShowInfo _value, $Res Function(_$_ShowInfo) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? breedInfo = null,
  }) {
    return _then(_$_ShowInfo(
      breedInfo: null == breedInfo
          ? _value.breedInfo
          : breedInfo // ignore: cast_nullable_to_non_nullable
              as BreedInfo,
    ));
  }
}

/// @nodoc

class _$_ShowInfo implements _ShowInfo {
  const _$_ShowInfo({required this.breedInfo});

  @override
  final BreedInfo breedInfo;

  @override
  String toString() {
    return 'BreedsState.showInfo(breedInfo: $breedInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowInfo &&
            (identical(other.breedInfo, breedInfo) ||
                other.breedInfo == breedInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, breedInfo);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ShowInfoCopyWith<_$_ShowInfo> get copyWith =>
      __$$_ShowInfoCopyWithImpl<_$_ShowInfo>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) {
    return showInfo(breedInfo);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) {
    return showInfo?.call(breedInfo);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) {
    if (showInfo != null) {
      return showInfo(breedInfo);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) {
    return showInfo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) {
    return showInfo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) {
    if (showInfo != null) {
      return showInfo(this);
    }
    return orElse();
  }
}

abstract class _ShowInfo implements BreedsState {
  const factory _ShowInfo({required final BreedInfo breedInfo}) = _$_ShowInfo;

  BreedInfo get breedInfo;
  @JsonKey(ignore: true)
  _$$_ShowInfoCopyWith<_$_ShowInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorStateCopyWith<$Res> {
  factory _$$_ErrorStateCopyWith(
          _$_ErrorState value, $Res Function(_$_ErrorState) then) =
      __$$_ErrorStateCopyWithImpl<$Res>;
  @useResult
  $Res call({Object? exception});
}

/// @nodoc
class __$$_ErrorStateCopyWithImpl<$Res>
    extends _$BreedsStateCopyWithImpl<$Res, _$_ErrorState>
    implements _$$_ErrorStateCopyWith<$Res> {
  __$$_ErrorStateCopyWithImpl(
      _$_ErrorState _value, $Res Function(_$_ErrorState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exception = freezed,
  }) {
    return _then(_$_ErrorState(
      exception: freezed == exception ? _value.exception : exception,
    ));
  }
}

/// @nodoc

class _$_ErrorState implements _ErrorState {
  const _$_ErrorState({this.exception});

  @override
  final Object? exception;

  @override
  String toString() {
    return 'BreedsState.error(exception: $exception)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ErrorState &&
            const DeepCollectionEquality().equals(other.exception, exception));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(exception));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorStateCopyWith<_$_ErrorState> get copyWith =>
      __$$_ErrorStateCopyWithImpl<_$_ErrorState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BreedInfo> fetchedDogs) loaded,
    required TResult Function(BreedInfo breedInfo) showInfo,
    required TResult Function(Object? exception) error,
  }) {
    return error(exception);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult? Function(BreedInfo breedInfo)? showInfo,
    TResult? Function(Object? exception)? error,
  }) {
    return error?.call(exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BreedInfo> fetchedDogs)? loaded,
    TResult Function(BreedInfo breedInfo)? showInfo,
    TResult Function(Object? exception)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitialState value) initial,
    required TResult Function(_LoadingState value) loading,
    required TResult Function(_LoadedState value) loaded,
    required TResult Function(_ShowInfo value) showInfo,
    required TResult Function(_ErrorState value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitialState value)? initial,
    TResult? Function(_LoadingState value)? loading,
    TResult? Function(_LoadedState value)? loaded,
    TResult? Function(_ShowInfo value)? showInfo,
    TResult? Function(_ErrorState value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitialState value)? initial,
    TResult Function(_LoadingState value)? loading,
    TResult Function(_LoadedState value)? loaded,
    TResult Function(_ShowInfo value)? showInfo,
    TResult Function(_ErrorState value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _ErrorState implements BreedsState {
  const factory _ErrorState({final Object? exception}) = _$_ErrorState;

  Object? get exception;
  @JsonKey(ignore: true)
  _$$_ErrorStateCopyWith<_$_ErrorState> get copyWith =>
      throw _privateConstructorUsedError;
}
