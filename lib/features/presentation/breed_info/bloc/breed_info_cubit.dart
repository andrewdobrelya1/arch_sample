import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'breed_info_state.dart';

class BreedInfoCubit extends Cubit<BreedInfoState> {
  BreedInfoCubit() : super(BreedInfoInitial());
}
